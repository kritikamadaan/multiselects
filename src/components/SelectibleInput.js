import React,{Component} from "react";
import Multiselect from 'multiselect-react-dropdown';

class SelectibleInput extends Component{   
     constructor(props) {
        super(props);

        this.state = {
            options: [{ name: 'CSS3', id: 1 }, { name: 'Javascript', id: 2 },
            { name: 'Bootstrap4', id: 3 }, { name: 'Reactjs', id: 4 },
            { name: 'Redux', id: 5 }, { name: 'Redux-Saga', id: 6},
            { name: 'React-native', id: 6 }, { name: 'Python', id: 6}
        ],
            selectedValue:""
        };
    }
    onSelect(selectedList, selectedItem) {
        console.log('selectedd item',selectedItem,selectedList)
    }
    onRemove(selectedList, removedItem) {
        console.log('selected remove',selectedList,removedItem)
    }
    render() {
        return (
            <div className="container-xl">
            <div className="multiSelect">
                <Multiselect
                    customCloseIcon={<>🍑</>}
                    caseSensitiveSearch
                    showCheckbox
                    placeholder="Select  Tags"
                    id="css_custom"
                    options={this.state.options} // Options to display in the dropdown
                    selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                    onSelect={this.onSelect} // Function will trigger on select event
                    onRemove={this.onRemove} // Function will trigger on remove event
                    displayValue="name" // Property name to display in the dropdown options
                    style={{
                        chips: {
                          background: 'white',
                          color:'black'
                        },
                        multiselectContainer: {
                          color: 'black'
                        },
                        searchBox: {
                          border: 'none',
                          'border-bottom': '2px solid blue',
                          'background':'#F5F5F5',
                          'border-radius': '0px',
                          'height':'100px'
                        }
                      }}
                />
            </div>
        </div>
        )
    }
}
export default SelectibleInput
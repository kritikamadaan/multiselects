
import SelectibleInput from './components/SelectibleInput';
import './App.css';

function App() {
  return (
    <div className="App">
     <SelectibleInput/>
    </div>
  );
}

export default App;
